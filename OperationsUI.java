import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Scanner;

import builders.CustomerSummaryBuilder;
import builders.EmployeeSummaryBuilder;
import builders.ProductSummaryBuilder;
import builders.XMLBuilder;
import pojo.CustomerList;
import pojo.OperationSummaryList;
import pojo.ProductList;
import pojo.StaffList;
import pojo.YearList;


public class OperationsUI {
	
	public static void main(String[] arg) {
		Scanner in = new Scanner(System.in);
        System.out.println("Enter start date for search:");
        String startDate = in.nextLine();
		while(!OperationsUI.isValidDate(startDate)) {
			System.out.println("Please enter start date in YYYY-MM-DD format");
			startDate = in.nextLine().trim();
		}
		
		
        System.out.println("Enter end date for search:");
        String endDate = in.nextLine().trim();
        
        while(!OperationsUI.isValidDate(endDate)) {
			System.out.println("Please enter end date in YYYY-MM-DD format");
			endDate = in.nextLine().trim();
		}
		
        
        System.out.println("Enter output file name without extension(.xml):");
        String fileName = in.nextLine().trim();

        while(fileName==null || fileName.equals("")  ) {
        	if(fileName==null || fileName.equals("") ) {
        	System.out.println("File name cannot be empty or null. Please enter a valid file name:");
        	fileName = in.nextLine().trim();
        	}
        	
        }
        
       
        
		
		//invokes customer summary builder for customer list
		CustomerList customerList  = new CustomerSummaryBuilder().generateCustomerSummary(startDate, endDate);
		
		//invokes product summary builder for product list
		ProductList productList = new ProductSummaryBuilder().generateProductSummary(startDate, endDate);

		//invokes staff summary builder for staff list
		StaffList staffList =  new EmployeeSummaryBuilder().generateEmployeeSummary(startDate, endDate);
		
		YearList yearList = new YearList();
		yearList.setStart_date(startDate);
		yearList.setEnd_date(endDate);
		OperationSummaryList operationSummaryList = new OperationSummaryList();
		operationSummaryList.setYear(yearList);
		operationSummaryList.setCustomer_list(customerList);
		operationSummaryList.setProduct_list(productList);
		operationSummaryList.setStaff_list(staffList);
		
		XMLBuilder builder = new XMLBuilder();
		//invokes XMLBuilder for xml conversion
		System.out.println("file added: "+builder.ConvertToXML(operationSummaryList, fileName));
		
		System.out.println("would you like to generate another record? Y/N :");
		String status = in.nextLine();
		if(status.trim().equals("Y") || status.trim().equals("y")) {
			String[] argq =new String[1];
			main(argq);
		}
		
	}
	//check if the given date is in valid format
	private static boolean isValidDate(String date){
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		try {
			dateFormat.setLenient(false);		

			dateFormat.parse(date);

		} catch (ParseException e) {
			return false;
		}
		return true;
	}
}
