package dao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pojo.Customer;
import pojo.CustomerList;
import pojo.Employee;
import pojo.Product;
import pojo.ProductLine;
import pojo.ProductList;
import pojo.StaffList;

public class DatabaseConnection {

	public CustomerList queryDbForCustomerSummary(String query) {
		Connection connect = null;
		Statement statement = null;
		ResultSet resultSet= null;
		CustomerList customerList = new CustomerList();
		try {
			//creating db instance to fetch customer records
			Class.forName("com.mysql.cj.jdbc.Driver");
			connect = DriverManager.getConnection("jdbc:mysql://db.cs.dal.ca:3306?serverTimezone=UTC", "sasidharan", "B00835818");
			statement = connect.createStatement();
			statement.execute("use csci3901;");
			resultSet= statement.executeQuery(query);

			
			if(resultSet != null) {
				
				List<Customer> customers = new ArrayList<Customer>();
				//sets the list of customer record to pojo
				while (resultSet.next()) {
					Customer cust = new Customer();
					cust.setCustomer_name( checkStringForNullForDb(resultSet.getString("customerName")));
					if(resultSet.getString("addressLine2")!=null) {
						cust.getAddress().setStreet_address( checkStringForNullForDb(resultSet.getString("addressLine1"))+" "+ resultSet.getString("addressLine2"));
					}else {
						cust.getAddress().setStreet_address( checkStringForNullForDb(resultSet.getString("addressLine1")));

					}
					cust.getAddress().setCity( checkStringForNullForDb(resultSet.getString("city")));
					cust.getAddress().setCountry( checkStringForNullForDb(resultSet.getString("country")));
					cust.getAddress().setPostal_code( checkStringForNullForDb(resultSet.getString("postalCode")));
					cust.setNum_orders( Integer.parseInt(checkIntForNullForDb(resultSet.getString("noOfOrders"))));
					cust.setOrder_value(Double.parseDouble(checkDoubleForNullForDb(resultSet.getString("orderValue"))));
					customers.add(cust);
				}
				customerList.setCustomer(customers);

			}
			resultSet.close();
			statement.close();
			connect.close();
		}
		catch (ClassNotFoundException e) {
			System.out.println("mysql driver not found.");
		}catch (SQLException e) {
			System.out.println("error while retreiving from database");
		}

	return customerList;
	}
	
	public ProductList queryDbForProductSummary(String query) {
		Connection connect = null;
		Statement statement = null;
		ResultSet resultSet= null;
		ProductList productList = new ProductList();
		try {
			//creating db instance to fetch product records
			Class.forName("com.mysql.cj.jdbc.Driver");
			connect = DriverManager.getConnection("jdbc:mysql://db.cs.dal.ca:3306?serverTimezone=UTC", "sasidharan", "B00835818");
			statement = connect.createStatement();
			statement.execute("use csci3901;");
			resultSet= statement.executeQuery(query);
			
			//sets the list of product record to pojo
			if(resultSet != null) {
		    	HashMap<String, List<Product>> productLineSpecific = new HashMap<String, List<Product>>();

		    	while (resultSet.next()) {

		        	Product product = new Product();
		        	product.setProduct_name(checkStringForNullForDb(resultSet.getString("productName")));
		        	product.setProduct_vendor(checkStringForNullForDb(resultSet.getString("productVendor")));
		        	product.setUnits_sold(Integer.parseInt(checkIntForNullForDb(resultSet.getString("unitsSold"))));
		        	product.setTotal_sales(Double.parseDouble(checkDoubleForNullForDb(resultSet.getString("totalValue"))));
		        	if(productLineSpecific.get(resultSet.getString("productLine"))==null) {
		        		List<Product> products = new  ArrayList<Product>();

		        		products.add(product);
		        		productLineSpecific.put(resultSet.getString("productLine"), products);
		        	}else {

		        		productLineSpecific.get(resultSet.getString("productLine")).add(product);
		        	}
		        	
		        }
		        
		        List<ProductLine> productLines = new ArrayList<ProductLine>();
		        for (Map.Entry<String, List<Product>> entry  : productLineSpecific.entrySet()) {
		        	ProductLine productLine = new ProductLine();
		        	productLine.setProduct_line_name(entry.getKey());
		        	productLine.setProduct(entry.getValue());
		            
		        	productLines.add(productLine);
				}
		        productList.setProduct_set(productLines);
		        resultSet.close();
				statement.close();
				connect.close();
		        
			}
		}catch (ClassNotFoundException e) {
			System.out.println("mysql driver not found.");
		}catch (SQLException e) {
			System.out.println("error while retreiving from database");
		}
		return productList;
	}
			
	public StaffList queryDbForEmployeeSummary(String query) {
				Connection connect = null;
				Statement statement = null;
				ResultSet resultSet= null;
				StaffList staffList = new StaffList();

				try {
					//creating db instance to fetch employee records
					Class.forName("com.mysql.cj.jdbc.Driver");
					connect = DriverManager.getConnection("jdbc:mysql://db.cs.dal.ca:3306?serverTimezone=UTC", "sasidharan", "B00835818");
					statement = connect.createStatement();
					statement.execute("use csci3901;");
					resultSet= statement.executeQuery(query);
					if(resultSet != null) {
				        List<Employee> employees = new ArrayList<Employee>();
						//sets the list of product record to pojo
				        while(resultSet.next()) {
				        	Employee employee = new Employee();
				        	employee.setFirst_name(checkStringForNullForDb(resultSet.getString("firstName")));
				        	employee.setLast_name(checkStringForNullForDb(resultSet.getString("lastName")));
				        	employee.setOffice_city(checkStringForNullForDb(resultSet.getString("city")));
				        	employee.setActive_customers(Integer.parseInt(checkIntForNullForDb(resultSet.getString("noOfActive"))));
				        	employee.setTotal_sales(Double.parseDouble(checkDoubleForNullForDb(resultSet.getString("totalOrderValue"))));
				        	employees.add(employee);
				        }
				        staffList.setEmployee(employees);
				       
				        
				        resultSet.close();
				        statement.close();
						connect.close();
				        
					}
				}catch (ClassNotFoundException e) {
					System.out.println("mysql driver not found.");
				}catch (SQLException e) {
					System.out.println("error while retreiving from database");
				}
				return staffList;
			}
	
	private String checkStringForNullForDb(String val){
		return val!=null? val : "";
		
	}
	private String checkIntForNullForDb(String val){
		return val!=null? val : "0";
		
	}
	private String checkDoubleForNullForDb(String val){
		return val!=null? val : "0.0";
		
	}


	
}
