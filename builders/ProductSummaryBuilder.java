package builders;
import dao.DatabaseConnection;
import pojo.ProductList;

public class ProductSummaryBuilder {
	private String startDate;
	private String endDate;
	DatabaseConnection dbConnection = new DatabaseConnection();
	public ProductList generateProductSummary(String startDate,String endDate) {
		this.startDate = startDate;
		this.endDate = endDate;

		String productSummaryQuery = "select pl.productLine,p.productName, p.productVendor,sum(od.quantityOrdered) as unitsSold,sum(od.quantityOrdered*od.priceEach) as totalValue from productlines pl inner join products p on p.productLine = pl.productLine inner join orderdetails od on od.productCode = p.productCode inner join orders ord on ord.orderNumber = od.orderNumber where ord.orderDate between '"+this.startDate+"' and '"+this.endDate+"' group by od.productCode;"; 

		//invokes database call method to generate product list resultset

		return this.dbConnection.queryDbForProductSummary(productSummaryQuery);



	}





}
