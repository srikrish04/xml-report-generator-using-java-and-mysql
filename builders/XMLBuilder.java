package builders;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import pojo.OperationSummaryList;

public class XMLBuilder {

	public boolean ConvertToXML(OperationSummaryList list,String fileLocation) {
		boolean status = false;
		try
		{
			StringWriter sw = new StringWriter();
			//creating new instance of jaxb
			JAXBContext jaxbContext = JAXBContext.newInstance(OperationSummaryList.class);

			//creating marshaller for xml conversion
			Marshaller jaxMarshaller = jaxbContext.createMarshaller();

			//setting jaxb property to format the output xml
			jaxMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

			// convert the list as xml in sw (string writer)
			jaxMarshaller.marshal(list,sw);

			String xmlContent = sw.toString();

			File file = new File(fileLocation+".xml");
			String filePath = file.getAbsolutePath();
			int i = filePath.lastIndexOf("\\");
			filePath = filePath.substring(0,i);
			File newFile = new File(filePath);

			//checks if the directory is not present. creates a new directory if not present
			if(!newFile.isDirectory() && file.getAbsolutePath().contains("\\")  ) {

				newFile.mkdirs();
			}
			//creates new file if file does not exist
			if (!file.exists()){

				file.createNewFile();
			}

			//writing file with string builder xml content
			FileWriter writer = new FileWriter(file);
			writer.write(xmlContent);
			writer.flush();
			writer.close();
			status = true;
			System.out.println("XML file created at "+file.getAbsolutePath());

		} catch (JAXBException e) {
			System.out.println("Error occured while creating XML");
		} catch (IOException e) {
			System.out.println("Directory not found. Please specify correct path. ");
		}
		return status;
	}
}
