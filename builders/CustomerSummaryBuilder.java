package builders;
import dao.DatabaseConnection;
import pojo.CustomerList;

public class CustomerSummaryBuilder {
	private String startDate;
	private String endDate;
	DatabaseConnection dbConnection = new DatabaseConnection();
	public CustomerList generateCustomerSummary(String startDate,String endDate) {
		this.startDate = startDate;
		this.endDate = endDate;
		
		String customerSummaryQuery = "select customerName, addressLine1, addressLine2, city, postalCode, country, count(od.orderNumber) as noOfOrders,sum(od.quantityOrdered*od.priceEach) as orderValue from customers c  inner join orders ord on c.customerNumber = ord.customerNumber inner join orderdetails od on ord.orderNumber = od.orderNumber where ord.orderDate between '"+ this.startDate+"' and '"+this.endDate+"' group by  ord.customerNumber;"; 
		
		//invokes database call method to generate customer list resultset
		return this.dbConnection.queryDbForCustomerSummary(customerSummaryQuery);
	}



}
