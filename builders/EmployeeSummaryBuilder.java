package builders;
import dao.DatabaseConnection;
import pojo.StaffList;

public class EmployeeSummaryBuilder {
	private String startDate;
	private String endDate;
	DatabaseConnection dbConnection = new DatabaseConnection();
	public StaffList generateEmployeeSummary(String startDate,String endDate) {
		this.startDate = startDate;
		this.endDate = endDate;

		String employeeSummaryQuery = "select e.firstName,e.lastName, o.city,count(distinct ord.customerNumber) as noOfActive,sum(od.quantityOrdered*od.priceEach) as totalOrderValue  from employees e inner join customers c on c.salesRepEmployeeNumber = e.employeeNumber inner join orders ord on c.customerNumber = ord.customerNumber inner join orderdetails od on ord.orderNumber = od.orderNumber inner join offices o on o.officeCode = e.officeCode where ord.orderDate between '"+this.startDate+"' and '"+this.endDate+"' group by e.employeeNumber;"; 

		//invokes database call method to generate employee list resultset
		return this.dbConnection.queryDbForEmployeeSummary(employeeSummaryQuery);



	}

}
