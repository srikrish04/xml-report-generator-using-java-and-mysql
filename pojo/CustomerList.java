package pojo;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "customer_list")
@XmlAccessorType (XmlAccessType.FIELD)
public class CustomerList {
List<Customer> customer = new ArrayList<Customer>();

public List<Customer> getCustomer() {
	return customer;
}

public void setCustomer(List<Customer> customer) {
	this.customer = customer;
}


}
