package pojo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "product")
@XmlAccessorType (XmlAccessType.FIELD)
public class Product {
String product_name;
String product_vendor;
int units_sold;
double total_sales;

public String getProduct_name() {
	return product_name;
}
public void setProduct_name(String product_name) {
	this.product_name = product_name;
}
public String getProduct_vendor() {
	return product_vendor;
}
public void setProduct_vendor(String product_vendor) {
	this.product_vendor = product_vendor;
}
public int getUnits_sold() {
	return units_sold;
}
public void setUnits_sold(int units_sold) {
	this.units_sold = units_sold;
}
public double getTotal_sales() {
	return total_sales;
}
public void setTotal_sales(double total_sales) {
	this.total_sales = total_sales;
}



}
