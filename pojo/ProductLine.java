package pojo;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
@XmlRootElement(name = "product_set")
@XmlAccessorType (XmlAccessType.FIELD)
public class ProductLine {
	String product_line_name;;
	List<Product> product = new ArrayList<Product>();
	public String getProduct_line_name() {
		return product_line_name;
	}
	public void setProduct_line_name(String product_line_name) {
		this.product_line_name = product_line_name;
	}
	public List<Product> getProduct() {
		return product;
	}
	public void setProduct(List<Product> product) {
		this.product = product;
	}
	
	
	
	
}
