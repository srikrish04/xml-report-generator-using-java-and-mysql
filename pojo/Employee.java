package pojo;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "employee")
@XmlAccessorType (XmlAccessType.FIELD)
public class Employee {
	String first_name;
	String last_name;
	String office_city;
	int active_customers;
	double total_sales;
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getOffice_city() {
		return office_city;
	}
	public void setOffice_city(String office_city) {
		this.office_city = office_city;
	}
	public int getActive_customers() {
		return active_customers;
	}
	public void setActive_customers(int active_customers) {
		this.active_customers = active_customers;
	}
	public double getTotal_sales() {
		return total_sales;
	}
	public void setTotal_sales(double total_sales) {
		this.total_sales = total_sales;
	}
	
	
	
}
