package pojo;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "product_list")
@XmlAccessorType (XmlAccessType.FIELD)
public class ProductList {
List<ProductLine> product_set  = new ArrayList<ProductLine>();

public List<ProductLine> getProduct_set() {
	return product_set;
}

public void setProduct_set(List<ProductLine> product_set) {
	this.product_set = product_set;
}


}
