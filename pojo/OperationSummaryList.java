package pojo;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "year_end_summary")
@XmlAccessorType (XmlAccessType.FIELD)

public class OperationSummaryList {
	YearList year = new YearList();
	CustomerList customer_list = new CustomerList(); 
	ProductList product_list = new ProductList();
	StaffList staff_list = new StaffList();
	public ProductList getProduct_list() {
		return product_list;
	}
	public void setProduct_list(ProductList product_list) {
		this.product_list = product_list;
	}
	public StaffList getStaff_list() {
		return staff_list;
	}
	public void setStaff_list(StaffList staff_list) {
		this.staff_list = staff_list;
	}
	public CustomerList getCustomer_list() {
		return customer_list;
	}
	public void setCustomer_list(CustomerList customer_list) {
		this.customer_list = customer_list;
	}
	public YearList getYear() {
		return year;
	}
	public void setYear(YearList year) {
		this.year = year;
	}
    
	
}
