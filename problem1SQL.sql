show databases;
use csci3901;

#a.Which customers are in a different city than their sales representative? 
 
 select c.customerName, c.city as customerCity, c.contactFirstName, c.contactLastName, e.employeeNumber, e.firstName,o.city as officeCity from customers c
 inner join employees e on c.salesRepEmployeeNumber = e.employeeNumber 
 inner join offices o on e.officeCode = o.officeCode where c.city != o.city;
 
 
#b. Which orders included sales that are below the manufacturer’s suggested retail price (MSRP)? 
 
 select distinct o.* from orders o inner join orderdetails od on o.orderNumber = od.orderNumber 
 inner join products p on od.productCode = p.productCode where od.priceEach < p.MSRP  ;
 
 
 
#c. List the top 5 products for 2004 with the highest average mark-up percentage per order.  Include the mark-up.  The mark-up is the ratio of (sale price – cost) and cost. 
 
 select p.*,o.orderDate,((avg(od.priceEach) - p.buyPrice)/p.buyPrice) as markup from products p 
 inner join orderdetails od on p.productCode = od.productCode 
 inner join orders o on od.orderNumber = o.orderNumber
 where YEAR(o.orderDate) = 2004 group by p.productCode order by markup desc limit 5;
 
 
#d. List the top 3 employees with the greatest average diversity of products in their orders.
 
 select count(distinct od.productCode) as distinctProductCount,e.* from employees e
 inner join customers c on e.employeeNumber = c.salesRepEmployeeNumber
 inner join orders o on o.customerNumber = c.customerNumber
 inner join orderdetails od on od.orderNumber = o.orderNumber 
 group by e.employeeNumber order by distinctProductCount desc limit 3 ;
 
#e. What is the average time needed to ship orders from each office in 2005, relative to the order date?  
 
 select o.*,avg(DATEDIFF(ord.shippedDate,ord.orderDate)) from offices o 
 inner join employees e on e.officeCode = o.officeCode 
 inner join customers c on c.salesRepEmployeeNumber = e.employeeNumber
 inner join orders ord on ord.customerNumber = c.customerNumber
 where YEAR(ord.orderDate) = 2005 group by e.officeCode;
 
 
 